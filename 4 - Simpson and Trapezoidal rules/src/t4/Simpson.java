package t4;
/*
 * File: Simpson.java
 */
class Simpson {
    Function f;
    double a, b, h, value;
    int n;

    Simpson(Function f, double a, double b, int n) {
        this.f = f;
        this.a = a;
        this.b = b;
        this.n = n;
        h = (b - a)/n;
        integrate();
    }

    private void
    integrate() {
        double evens = summation(2, n - 2, 2);
        double odds = summation(1, n - 1, 2);
        value = (h/3)*(f.function(a) + f.function(b) + 2 * evens +
                       4 * odds);
    }

    private double
    summation(int init, int stop, int step) {
        double total = 0;
        for(int i = init; i <= stop; i += step) {
            total += f.function(a + i * h);
        }
        return total;
    }

    double getValue() {
        return value;
    }
}
