package t4;
/*
 * File: Trapezio.java
 */
import java.lang.Math;

class Trapezio {
    Function f;
    double a, b, h;
    double I = 
    1.05484187724911547751533517464219602152206870774592276179752284037644091143765440923297387253623012796377251858496919472367147923666436460017169769843;

    Trapezio(Function f, double a, double b) {
        this.f = f;
        this.a = a;
        this.b = b;
        integrate();
    }

    private void
    integrate() {
		double summation = 0;
		double h = (b - a)/2; // n inicial = 2
		double fa_fb = f.function(a) + f.function(b);
		
        for( int k = 1; k <= 20; ++k ){
			double partial_sum = 0; // Guarda sumatorio dos pontos novos
			int n = (int)Math.pow(2, k);
			for(int i = 1; i < n; i += 2){
				partial_sum += f.function(a + i * h);
			}
			summation += partial_sum;
			double value = (h/2)*fa_fb + h * summation; 
			System.out.println(k + "\t| " + value + "\t| " + (I - 
			value) );
			h /= 2; 
		}
    }
}
