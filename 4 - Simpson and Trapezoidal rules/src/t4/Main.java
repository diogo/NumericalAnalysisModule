package t4;
/*
 * Main.java
 */
import java.lang.Math;

public class Main {
        static int A = 0;
	static int B = 2;

	public static void main (String[] args) {
		Function f = new Functions(1);
		Simpson s = new Simpson(f, 0, 2, calculateN(Math.pow(10, -7)));
		System.out.println(s.getValue());
		s = new Simpson(f, 0, 2, calculateN(Math.pow(10, -7)));
		System.out.println(s.getValue());
		s = new Simpson(f, 0, 2, calculateN(Math.pow(10, -12)));
		System.out.println(s.getValue());
		Trapezio t = new Trapezio(f, 0, 2);

	}

	private static int
	calculateN(double error) {
		int n = (int)Math.ceil( (B - A)/fourthRoot( (15*error)/2 ) );
		if (n%2 != 0) n++;
		return n;
	}

	private static double
	fourthRoot(double x) {
		return Math.pow(Math.E, Math.log(x)/(4));
	}
}
