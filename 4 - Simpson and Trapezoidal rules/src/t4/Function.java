package t4;
/*
 *	Function.java
 *	Defines an interface to be implemented by a function
 */
interface Function {
	double function(double x);
}
