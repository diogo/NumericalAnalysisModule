package t4;
import java.lang.Math;

class Functions implements Function {
    int f;
    Functions (int f) { this.f = f; }

	public double function(double x) {
		switch(f) {
		case 1:
			return Math.sin(Math.sin(Math.sin(Math.sin(x))));
		case 2:
			return Math.exp(x);
		}
		return 0;
    }
}
