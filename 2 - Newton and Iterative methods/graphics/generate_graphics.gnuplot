#!/usr/bin/gnuplot
set output "err_fixed_point.png";
set terminal png size 1200,900 enhanced font "Helvetica,20"
set autoscale;
set logscale x;
set xlabel "Erro"
set ylabel "Iterações"
set xrange [] reverse;
set linetype 1 lc rgb 'black'
set linetype 2 lc rgb 'black
plot "err_fixed_point.txt" with points pointtype 1 pointsize 2 lw 4 t "Fixed Point", "err_newton.txt" with points pointtype 2 pointsize 2 lw 4 t "Newton";

set terminal png size 1200,900 enhanced font "Helvetica,35"
set output "graph.png";
unset logscale x;
set xr [-1:1];
set zeroaxis ls 1 lw 3;
set xlabel "x"
set ylabel "y"

f(x)= x**2 - cos(x)**2;
plot f(x) lw 3 t "x^2 - cos(x)^2";

set output "graph_small.png";

set xr [0.7:0.8];
plot f(x) lw 3 t "x^2 - cos(x)^2";