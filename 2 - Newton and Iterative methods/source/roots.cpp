#include <iostream>
#include <cmath>
#include <iomanip>

template<typename step_func>
double find_root(double x0, double epsilon, step_func step, long iter_limit) {
	double x1 = x0, err;
	long iter = 1;
	std::cout << "Erros: ";
	do {
		x0 = x1;
		x1 = step(x0);
		err = std::abs(x1 - x0);

		std::cout << iter << " "
		          << std::setprecision(1) << std::scientific << err << " "
		          << std::defaultfloat << std::setprecision(12) << x1 << '\n';
	} while(err > epsilon && iter++ < iter_limit);
	std::cout << '\n';
	// std::cout << std::defaultfloat << std::setprecision(12);
	return x1;
}

template<typename F_t, typename dF_t>
double newton(double x0, double epsilon, F_t F, dF_t dF, long iter_limit) {
	return find_root(x0, epsilon, [&F, &dF](double x0){ return x0 - F(x0)/dF(x0); }, iter_limit);
}

template<typename F_t>
double fixed_point(double x0, double epsilon, F_t F, long iter_limit) {
	return find_root(x0, epsilon, F, iter_limit);
}

int main() {
	auto F = [](double x){ return std::pow(x, 2.0) - std::pow(std::cos(x), 2.0); };
	auto dF = [](double x){ return 2.0 * x + std::sin(2.0 * x); };
	// std::cout.precision(17);

	double epsilon = 5.0 * std::pow(10.0, -12.0);

	std::cout << newton(0.8, epsilon, F, dF, 100000) << '\n';

	auto F2 = [](double x){ return std::cos(x); };
	std::cout << fixed_point(0.8, epsilon, F2, 100000) << '\n';
}
