#!/usr/bin/gnuplot

set output "a.png";
set terminal png size 1200,900 enhanced font "Helvetica, 20";

plot "a_polynomial.txt" with l lw 4 t "Polinomial", "a_spline.txt" with l lw 4 t "Spline Cúbico";

set output "b.png";

f(x) = 4*x**2 + sin(9*x)
plot f(x) with l lw 4 t "f(x)", "b_polynomial.txt" with l lw 4 t "Polinomial", "b_spline.txt" with l lw 4 t "Spline Cúbico";