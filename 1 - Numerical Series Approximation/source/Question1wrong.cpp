#include <iostream>

double
machine_eps(double start)
{
	double epsilon = 1.0f;

	while ((start + 0.5f * epsilon) != start)
        {
		epsilon = 0.5f * epsilon;
	}

	return epsilon;
}

int
main()
{
	std::cout.precision(17);

        std::cout << machine_eps(1) << " - macheps" << '\n';
}
