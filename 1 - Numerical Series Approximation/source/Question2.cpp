#include <iostream>
#include <cmath>
#include <cstdlib>


/**
 * Compute k term of Serie 2 given the previous one
 */
double _compute_serie_2_term(unsigned long k, double ak) {
	return double(k+1)/(double(4*k+6)) * ak;
}

// double c(int k) {
// 	return std::pow(std::tgamma(k + 1), 2.0)/std::tgamma(2*k + 2);
// }

/**
 * Compute serie 2 with absole error below a given epsilon
 */
void compute_serie_2(double epsilon) {
	// Constant factor
	double factor = 9.0/(2.0*std::sqrt(3));
	// Taken from D'Alembert criterion for given that L = 0.5 < 1
	double super_L = 1.0/(1.0-0.25);

	// Summation index
	unsigned long k = 0;
	// Acumulation for the Summation
	double acc = 0;

	// Previous and current value on the series
	// double a_prev = 1.0f, // a with k = 0
	double a = 1.0; // _compute_serie_2_term(k, a_prev); // compute next
	// While our absolute error is higher than the given epsilon
	while(epsilon < factor*a*super_L) {
		// Accumulate with previous term

		acc += a;
		// Update previous term to current term
		// a_prev = a;
		// Compute current term
		// a = c(k);
		a = _compute_serie_2_term(k++, a);
	}

	std::cout << k << " " << std::abs(M_PI - factor*acc) << '\n';
	std::cout << factor * acc << " | " << k;
}



int main(int, char **argv) {
	std::cout.precision(17);
	double power_of_ten = std::pow(10.0, -std::atoi(argv[1]));
	compute_serie_2(power_of_ten);
}
