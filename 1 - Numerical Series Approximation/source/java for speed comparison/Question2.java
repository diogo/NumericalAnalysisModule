public class Question2 {

        public static void main(String[] args) {
	        double power_of_ten = Math.pow(10,-1 * Integer.parseInt(args[0]));
	        compute_serie_2(power_of_ten);
        }

        /**
         * Compute serie 2 with absole error below a given epsilon
         */
        public static void
        compute_serie_2(double epsilon)
        {
                // Constant factor
                double factor = 9.0f/(double)(2.0f*Math.sqrt(3));
                // Taken from D'Alembert criterion for given that L = 0.5 < 1
                double super_L = 1.0f/(double)(1-0.25);

                // Summation index
                int k = 0;
                // Acumulation for the Summation
                double acc = 0;

                // Previous and current value on the series
                double a = 1.0f; // a with k = 0
                // While our absolute error is higher than the given epsilon
                while(epsilon < factor*a*super_L)
                {
                        // Accumulate with previous term
                        acc += a;
                        // Compute current term
                        a = _compute_serie_2_term(k++) * a;
                }
                // System.out.println(epsilon);
                // System.out.println("Value: " + factor*acc);
                // System.out.println("Error: " + (3.1415926535897932 - factor*acc));
                // System.out.println("K: " +(k-1) + "\n--------------");
                System.out.println(factor*acc + " " + (k - 1));
        }

        /**
         * Compute k term of Serie 2 given the previous one
         */
        public static double
        _compute_serie_2_term(int k)
        {
                return (double)(k+1.0f)/(double)(4.0f*k+6.0f);
        }
}
