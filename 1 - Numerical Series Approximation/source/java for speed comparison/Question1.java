public class Question1 {

	public static void main(String args[]) {
		System.out.println(machine_eps());
	}

	public static float machine_eps() {
		float epsilon = 1.0f;

		while ((1.0f + 0.5f * epsilon) != 1.0f){
			epsilon = 0.5f * epsilon;
		}

		return epsilon;
	}
}
