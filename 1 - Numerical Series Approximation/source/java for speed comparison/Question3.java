public class Question3 {

    public static void main(String[] args) {
	    double err = Math.pow(10, -Integer.parseInt(args[0].trim()));
	    compute_serie_2(err);
    }

    public static double compute_serie_3_term(long n)
    {
        return -((double)(2*n+1))/(double)(2*n+3);
    }

    public static void compute_serie_2(double err)
    {
        long k = 0;
        double ak = 1;
        double acc = 1;

        while (err < 4 * Math.abs(ak)) {
	        ak = compute_serie_3_term(k++) * ak;
	        acc += ak;
        }

        System.out.println(k + " " + 4 * acc);
    }

}

