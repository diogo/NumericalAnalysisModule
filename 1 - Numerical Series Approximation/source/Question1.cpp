#include <iostream>

double machine_eps() {
	double epsilon_candidate = 2.0,
 	       epsilon = epsilon_candidate,
	       power = 2.0;

	while (epsilon_candidate != 1.0)
	{
		epsilon = epsilon_candidate;
		epsilon_candidate -= 1/power;
		power *= 2;
	}

	return epsilon-1.0;
}

int main() {
	std::cout.precision(17);

	std::cout << machine_eps() << " - macheps" << '\n';

	/*for(double step = 0.0f; step < 100'000'000; step += 1.0)
	{
		std::cout << step << " " << machine_eps(step) << '\n';
	}*/
}
