#include <iostream>
#include <cmath>
#include <numeric>
#include <ios>
#include <cstdlib>

double compute_serie_3_term(unsigned long n) {
	return -((double)(2*n+1))/(double)(2*n+3);
}

double compute_serie_3(double err) {

	unsigned long k = 0;
	double ak = 1;
	double acc = 1;

	while (err < 4*std::abs(ak)) {
		ak *= compute_serie_3_term(k++);
		acc += ak;
	}

	std::cout << k << " " << 4*acc << " " << std::abs(3.14159265358979324$$ - 4*acc) << '\n';
	return 4*acc;
}


int main(int, char **argv) {
	const double err = std::pow(10.0, -std::atof(argv[1]));
	std::cout.precision(17);
	compute_serie_3(err);
}
