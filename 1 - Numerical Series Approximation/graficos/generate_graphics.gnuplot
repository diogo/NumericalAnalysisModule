#!/usr/bin/gnuplot
set output "questao3_tempos.png";
set terminal png size 1200,900 enhanced font "Helvetica,20"
set autoscale;
set xr [7:13];
set logscale y;
set xlabel "-log(epsilon)"
set ylabel "Tempo (s)"
set linetype 1 lc rgb 'black'
set linetype 2 lc rgb 'black
plot "../questao3_tempo_java.txt" with linespoints dashtype 0 lw 4 t "Java", "../questao3_tempo_Cpp.txt" with linespoints dashtype 2 lw 4 t "C++";

set output "questao3_iteracoes.png";
set terminal png size 1200,900 enhanced font "Helvetica,20"
set autoscale;
set xr [7:13];
set logscale y;
set xlabel "-log(epsilon)"
set ylabel "Iterações"
set linetype 1 lc rgb 'black'
set linetype 2 lc rgb 'black
plot "../questao3_iteracoes_java.txt" with linespoints dashtype 0 lw 4 t "Java", "../questao3_iteracoes_Cpp.txt" with linespoints dashtype 2 lw 4 t "C++";

